# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 12:01:17 2020

@author: Anthony Aoun
"""

from flask import Flask, render_template, request
# from flaskwebgui import FlaskUI
import subprocess
import os
import requests
import json
import ast

my_path = os.path.abspath(os.path.dirname(__file__))
cmd = os.path.join(my_path, "../Simulator/Simulator/Build/Simulator.exe")

app = Flask(__name__)


# ui = FlaskUI(app)

@app.route('/')
def index():
    return render_template('index.html', header='Humantech VR simulator', sub_header='Please choose a simulation',
                           list_header="Simulations:",
                           galaxies=get_maps(), main_title="VR in healthcare project",
                           site_title="Humantech VR simulator")


def get_maps():
    response = requests.get("http://localhost:6003/map/listall")
    dictio = json.loads(response.content.decode("UTF-8"))
    print(type(dictio))
    return dictio["maps"]


@app.route('/hello')
def hello():
    simulationName = request.args.get('simulationName')
    response = requests.post("http://localhost:6003/map/", {"name": simulationName})
    with open('../Simulator/Simulator/LevelData/test.json', 'w') as outfile:
        json.dump(json.loads(response.content.decode("UTF-8")), outfile)

    process = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    process.wait()
    return render_template('index.html', header='Humantech VR simulator', sub_header='Please choose a simulation',
                           list_header="Simulations:",
                           galaxies=get_maps(), main_title="VR in healthcare project",
                           site_title="Humantech VR simulator")


if __name__ == "__main__":
    # ui.run()
    app.run()