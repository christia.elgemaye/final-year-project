const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var pos = new Schema({
    x: {
        type: Number,
        required: true
    },
    y: {
        type: Number,
        required: true
    },
    z: {
        type: Number,
        required: true
    }
}, {_id: false})

var rot = new Schema({
    x: {
        type: Number,
        required: true
    },
    y: {
        type: Number,
        required: true
    },
    z: {
        type: Number,
        required: true
    },
    w: {
        type: Number,
        required: true
    }
}, {_id: false})

var editorObjects = new Schema({
    pos: {
        type: pos,
        required: true
    },
    rot: {
        type: rot,
        required: true
    },
    objectType: {
        type: String,
        required: true
    },

    scale: {
        type: pos,
        required: true
    },
    scripts: [{
        String
    }],
    patrol: {
        type: String,
        required: true
    },

    posPatrol: {
        type: pos
    },
    scalPatrol: {
        type: pos,
        required: false
    },
    route: {
        type: Boolean
    },

    routeWaypoints: [{
        pos
    }],

    triggerTimer: {
        type: Number
    },
    
    range: {
        type: Number
    },

    spotAngle: {
        type: Number
    },

    intensity: {
        type: Number
    },

    R: {
        type: Number
    },

    G: {
        type: Number
    },

    B: {
        type: Number
    },

    A: {
        type: Number
    },

    item: {
        type: String
    },

    tag: {
        type: String

    },

    trigger: {
        type: String
    },

    textTriggered: {
        type: String
    }
}, {_id: false});

var questObjects = new Schema ({
    "questTitle": String,
    "questDesc": String,
    "questType": String,
    "x": Number,
    "y": Number,
    "z": Number
}, {_id: false}, {__v: false});

const maps = new Schema({
    name: {
        type: String,
        required: true
    },
    editorObjects: {
        type: [editorObjects],
        required: true
    },
    questObjects: {
        type: [questObjects],
        required: false
    }
}, {
    collection: 'maps'
}, {_id: false}, {__v: false});

module.exports = mongoose.model('maps', maps);