﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MouseScript : MonoBehaviour
{
    public enum LevelManipulation { Create, Rotate, Select, Resize, Rescale, Reposition, SelectPatrol, SelectRoute, AddScript, AddTeleport, ChangeColor, ChangeLight, AddTrigger }; // the possible level manipulation types

    [HideInInspector]
    public LevelManipulation manipulateOption = LevelManipulation.Select; // create is the default manipulation type.
    [HideInInspector]
    public MeshRenderer mr;
    [HideInInspector]
    public GameObject rotObject;
    [HideInInspector]
    public GameObject resizeObject;

    public Material goodPlace;
    public Material badPlace;
    public ManagerScript managerScript;

    private Vector3 mousePos;
    private bool colliding;
    private Ray ray;
    private RaycastHit hit;

    // Start is called before the first frame update
    void Start()
    {
        mr = GetComponent<MeshRenderer>(); // get the mesh renderer component and store it in mr.
    }

    // Update is called once per frame
    void Update()
    {
        // Have the object follow the mouse cursor by getting mouse coordinates and converting them to world point.

        if (Physics.Raycast(ray, out hit))
            mousePos = new Vector3(hit.point.x, hit.point.y, hit.point.z);
        //mousePos = Camera.main.ScreenToWorldPoint(mousePos);
        transform.position = new Vector3(
            Mathf.Clamp(mousePos.x, -20, 20),
            Mathf.Clamp(mousePos.y, -20, 20),
            Mathf.Clamp(mousePos.z, -20, 20)); // limit object movement to minimum -20 and maximum 20 for both x and z coordinates. Y alwasy remains 0.75.

        ray = Camera.main.ScreenPointToRay(Input.mousePosition); // send out raycast to detect objects
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.gameObject.layer == 9 || hit.collider.gameObject.layer == 10) // check if raycast hitting user created object.
            {
                colliding = true; // Unity now knows it cannot create any new object until collision is false.
                mr.material = badPlace; // change the material to red, indicating that the user cannot place the object there.
            }
            else
            {
                colliding = false;
                mr.material = goodPlace;
            }
        }

        // after pressing the left mouse button...
        if (Input.GetMouseButtonDown(0))
        {
            if (!EventSystem.current.IsPointerOverGameObject()) // check if mouse over UI object.
            {
                if (colliding == false && manipulateOption == LevelManipulation.Create) // create an object if not colliding with anything.
                    managerScript.objectCreator.CreateObject();
                else if (colliding == true && manipulateOption == LevelManipulation.Rotate) // Select object under mouse to be rotated.
                    SetRotateObject();
            }
        }
    }




    /// <summary>
    /// Object rotation
    /// </summary>
    void SetRotateObject()
    {
        if (Physics.Raycast(ray, out hit))
        {
            rotObject = hit.collider.gameObject; // object to be rotated
            managerScript.rotSlider.value = rotObject.transform.rotation.y; // set slider to current object's rotation.
        }
    }



}
