﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCreator : MonoBehaviour
{
    public Dictionary<string, GameObject> itemDict = new Dictionary<string, GameObject>();
    public Dictionary<string, Mesh> meshDict = new Dictionary<string, Mesh>();
    public Dictionary<string, Sprite> iconDict = new Dictionary<string, Sprite>();
    public List<string> itemNames;
    public List<GameObject> itemPrefabs;
    public List<Mesh> itemMeshes;
    public List<Sprite> itemIcons;

    [HideInInspector]
    public string itemOptionSelection = "Player";

    public ManagerScript managerScript;


    private void Awake()
    {
        for (int i = 0; (i < itemNames.Count) || (i < itemPrefabs.Count || (i < itemMeshes.Count)); i++)
        {
            itemDict.Add(itemNames[i], itemPrefabs[i]);
            meshDict.Add(itemNames[i], itemMeshes[i]);
            iconDict.Add(itemNames[i], itemIcons[i]);
        }
    }


    public void CreateObject()
    {
        if (itemOptionSelection.Equals("Default"))
        {
            return;
        }
        foreach (KeyValuePair<string, GameObject> entry in itemDict)
        {
            if (itemOptionSelection.Equals(entry.Key))
            {
                if (itemOptionSelection.Equals("Player"))
                {
                    if (managerScript.playerPlaced == false)
                        managerScript.playerPlaced = true;
                    else
                        break;
                }
                GameObject newObj = createPrefabObject(entry.Value);
                string objType = entry.Key;
                AddEditorData(newObj, objType);
                newObj.tag = "UserCreated";
                break;
            }
        }
    }


    private GameObject createPrefabObject(GameObject prefab)
    {
        GameObject newObj = Instantiate(prefab, new Vector3(managerScript.mouseScript.transform.position.x, prefab.transform.position.y, managerScript.mouseScript.transform.position.z), prefab.transform.rotation);
        newObj.layer = 9; // set to Spawned Objects layer
        return newObj;
    }


    public static void AddEditorData(GameObject newObj, string objType)
    {
        //Add editor object component and feed it data.
        EditorObject eo = newObj.AddComponent<EditorObject>();
        eo.data.pos = newObj.transform.position;
        eo.data.rot = newObj.transform.rotation;
        eo.data.scale = newObj.transform.localScale;
        eo.data.objectType = objType;
    }
}
