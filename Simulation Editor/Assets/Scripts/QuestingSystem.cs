﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestingSystem : MonoBehaviour
{
    public GameObject Button_Template;

    public List<QuestObject> questLog = new List<QuestObject>();

    // Start is called before the first frame update
    private void Start()
    {
        initList();
    }

    public void UpdateList()
    {
        GameObject go = Instantiate(Button_Template) as GameObject;
        go.SetActive(true);
        TutorialButton TB = go.GetComponent<TutorialButton>();
        TB.SetName(questLog[questLog.Count - 1].mydata.questTitle);
        go.tag = "Quest";
        go.transform.SetParent(Button_Template.transform.parent, false);
    }

    public void initList()
    {
        foreach (QuestObject str in questLog)
        {
            GameObject go = Instantiate(Button_Template) as GameObject;
            go.SetActive(true);
            TutorialButton TB = go.GetComponent<TutorialButton>();
            TB.SetName(str.mydata.questTitle);
            go.tag = "Quest";
            go.transform.SetParent(Button_Template.transform.parent, false);
        }
    }
}
