﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollView : MonoBehaviour
{
    int buttonSize = 80;
    int numberOfButtons = 21;
    public RectTransform Content;


    // Start is called before the first frame update
    void Start()
    {
        if (numberOfButtons < 12)
        {
            numberOfButtons = 1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Content.offsetMax.y < 0)
        { //It seems that is checking for less than 0, but the syntax is weird
            Content.offsetMax = new Vector2(); //Sets its value back.
            Content.offsetMin = new Vector2(); //Sets its value back.
        }
        if (Content.offsetMax.y > (numberOfButtons * buttonSize) - buttonSize)
        { // Checks the values
            Content.offsetMax = new Vector2(0, (numberOfButtons * buttonSize) - buttonSize); // Set its value back
            Content.offsetMin = new Vector2(); //Depending on what values you set on your scrollview, you                                              might want to change this, but my one didn't need it.
        }
    }
}