﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.AI;

public class PatrolArea : MonoBehaviour
{

    public ManagerScript managerScript;
    public Material patrolAreaMaterial;
    [HideInInspector]
    public Dictionary<GameObject, GameObject> patrolAreas = new Dictionary<GameObject, GameObject>();

    private RaycastHit hit;
    private int i = 0;
    private RaycastHit First;
    private GameObject cube;
    private bool placed = false;
    private float x;
    private float z;
    private bool willMove;
    private NavMeshAgent navMeshAgent;
    private GameObject patrolArea;
    private Animator animator;

    // Update is called once per frame
    void Update()
    {
        //StartCoroutine("patrol");
        if (managerScript.mouseScript.manipulateOption == MouseScript.LevelManipulation.SelectPatrol && !EventSystem.current.IsPointerOverGameObject())
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (placed)
                {
                    placed = false;
                    cube.gameObject.SetActive(false);
                }
                //Create a ray from the camera to our space
                var camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                
                if (Physics.Raycast(camRay, out hit))
                {
                    if (i == 0)
                    {
                        First = hit;
                    }
                    else
                    {
                        cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        cube.transform.position = new Vector3((hit.point.x + First.point.x)/2, 0, (hit.point.z + First.point.z)/2);
                        cube.transform.localScale = new Vector3(Mathf.Abs(hit.point.x - First.point.x), 1, Mathf.Abs(hit.point.z - First.point.z));
                        cube.gameObject.GetComponent<Renderer>().material = patrolAreaMaterial;
                        MeshRenderer meshRenderer = cube.gameObject.GetComponent<MeshRenderer>();
                        meshRenderer.receiveShadows = false;
                        meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

                        placed = true;
                    }
                    i = i == 0 ? 1 : 0;
                }
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0) && placed && !EventSystem.current.IsPointerOverGameObject())
            {
                placed = false;
                cube.SetActive(false);
            }
        }
    }
    
    //add to change it if it already exists DONE
    public void bind()
    {
        if (placed && managerScript.selectionManager.selectedUnits.Count > 0)
        {
            foreach (Transform j in managerScript.selectionManager.selectedUnits){
                if (patrolAreas.ContainsKey(j.gameObject))
                {
                    patrolAreas.Remove(j.gameObject);
                }
                patrolAreas.Add(j.gameObject, cube);

                j.GetComponent<EditorObject>().data.patrol = true;
                j.GetComponent<EditorObject>().data.scalPatrol = cube.transform.localScale;
                j.GetComponent<EditorObject>().data.posPatrol = cube.transform.position;


                if (!j.gameObject.TryGetComponent<NavMeshAgent>(out navMeshAgent))
                {
                    navMeshAgent = j.gameObject.AddComponent<NavMeshAgent>();
                    navMeshAgent.speed = 1.5f;
                    navMeshAgent.acceleration = 8;
                    navMeshAgent.angularSpeed = 10000;
                }
            }
        }
    }

    void patrol()
    {
        if (patrolAreas.Count == 0) return;
        foreach (GameObject item in patrolAreas.Keys)
        {

            navMeshAgent = item.GetComponent<NavMeshAgent>();
            if (item.gameObject.TryGetComponent<Animator>(out animator))
            {
                if (navMeshAgent.remainingDistance > navMeshAgent.stoppingDistance)
                {
                    animator.SetBool("isWalking", true);
                }
                else
                {
                    animator.SetBool("isWalking", false);
                }
            }


            if ((navMeshAgent.destination - item.transform.position).sqrMagnitude <= 5) navMeshAgent.acceleration = 500;
            if ((navMeshAgent.destination - item.transform.position).sqrMagnitude >= 8) navMeshAgent.acceleration = 8;
            if ((navMeshAgent.destination - item.transform.position).sqrMagnitude >= 5) continue;

            patrolAreas.TryGetValue(item, out patrolArea);

            x = Random.Range(patrolArea.transform.position.x - (patrolArea.transform.localScale.x/2), patrolArea.transform.position.x + (patrolArea.transform.localScale.x / 2));
            z = Random.Range(patrolArea.transform.position.z - (patrolArea.transform.localScale.z / 2), patrolArea.transform.position.z + (patrolArea.transform.localScale.z / 2));
            willMove = Random.Range(0f, 1f) < 0.001f ? true : false;
            if (willMove == true)
            {
                navMeshAgent.isStopped = true;
                navMeshAgent.SetDestination(new Vector3(x, 1, z));
                navMeshAgent.isStopped = false;
                
            }
        }
    }

}
