﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera2Script : MonoBehaviour
{
    private Camera cam;
    private float xAxis;
    private float yAxis;
    private bool editing = false;

    private void Start()
    {
        cam = GetComponent<Camera>();
    }

    void movement()
    {

        xAxis = Input.GetAxis("Horizontal"); // get user input
        yAxis = Input.GetAxis("Vertical");

        // move camera based on info from xAxis and yAxis
        transform.Translate(new Vector3(xAxis/10, 0, yAxis / 10));

        if (editing == false)
            cam.transform.Rotate(new Vector3(-Input.GetAxis("Mouse Y")*2, Input.GetAxis("Mouse X")*4, 0) * Time.deltaTime * 100);
    }

    // Update is called once per frame
    void Update()
    {
        movement();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            editing = editing ? editing = false : editing = true;
        }
    }
}
