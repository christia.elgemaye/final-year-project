﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SelectionManager : MonoBehaviour
{
    RaycastHit hit;
    bool isDragging = false;
    Vector3 mousePosition;
    public List<Transform> selectedUnits = new List<Transform>();
    public Shader outliner;
    public ManagerScript managerScript;

    private void OnGUI()
    {
        if (isDragging)
        {
            var rect = ScreenHelper.GetScreenRect(mousePosition, Input.mousePosition);
            ScreenHelper.DrawScreenRect(rect, new Color(0.8f, 0.8f, 0.95f, 0.1f));
            ScreenHelper.DrawScreenRectBorder(rect, 1, Color.blue);
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && managerScript.mouseScript.manipulateOption == MouseScript.LevelManipulation.Select && !EventSystem.current.IsPointerOverGameObject())
        {
            mousePosition = Input.mousePosition;
            //Create a ray from the camera to our space
            var camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(camRay, out hit))
            {
                if (hit.transform.CompareTag("UserCreated"))
                {
                    SelectUnit(hit.transform, Input.GetKey(KeyCode.LeftShift));
                }
                isDragging = true;
            }
        }

        if (Input.GetMouseButtonUp(0) && managerScript.mouseScript.manipulateOption == MouseScript.LevelManipulation.Select)
        {
            DeselectUnits();

            foreach (var selectableObject in FindObjectsOfType<Collider>())
            {
                if (IsWithinSelectionBounds(selectableObject.transform) && selectableObject.CompareTag("UserCreated"))
                {
                    SelectUnit(selectableObject.transform, true);
                }
            }
            mousePosition = Input.mousePosition;
            //Create a ray from the camera to our space
            var camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(camRay, out hit))
            {
                if (hit.transform.CompareTag("UserCreated"))
                {
                    SelectUnit(hit.transform, Input.GetKey(KeyCode.LeftShift));
                }
                isDragging = false;
            }
        }
        if (Input.GetKeyDown(KeyCode.Delete))
        {
            DeleteUnits();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            managerScript.ChooseSelect();
        }
    }

    private void SelectUnit(Transform unit, bool isMultiSelect = false)
    {
        if (!isMultiSelect)
        {
            DeselectUnits();
        }
        selectedUnits.Add(unit);
        //unit.Find("Highlight").gameObject.SetActive(true);
        Renderer renderer;
        if (unit.gameObject.TryGetComponent<Renderer>(out renderer)) {
            renderer.material.shader = outliner;
        }
    }

    public void DeselectUnits()
    {
        for (int i = 0; i < selectedUnits.Count; i++)
        {
            //selectedUnits[i].Find("Hightlight").gameObject.SetActive(false);
            Renderer renderer;
            if(selectedUnits[i].gameObject.TryGetComponent<Renderer>(out renderer)) {
            renderer.material.shader = Shader.Find("Standard");
        }

        }
        selectedUnits.Clear();
    }

    private bool IsWithinSelectionBounds(Transform transform)
    {
        var camera = Camera.main;
        var viewportBounds = ScreenHelper.GetViewportBounds(camera, mousePosition, Input.mousePosition);
        return viewportBounds.Contains(camera.WorldToViewportPoint(transform.position));
    }

    private void DeleteUnits()
    {
        for (int i = 0; i < selectedUnits.Count; i++)
        {
            Destroy(selectedUnits[i].gameObject);
            managerScript.patrolArea.patrolAreas.Remove(selectedUnits[i].gameObject);
        }
        selectedUnits.Clear();
    }
}